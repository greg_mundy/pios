#define GPIO_BASE       0x20200000UL
#define GPIO_GPFSEL1    1
#define GPIO_GPCLR0     10

volatile unsigned int* gpio;

int main(void) {
    gpio = (unsigned int*)GPIO_BASE;
    gpio[GPIO_GPFSEL1] |= (1 << 18);
    gpio[GPIO_GPCLR0] = (1 << 16);

    while(1) {
    }
}
